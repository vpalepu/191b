## Introduction ##

This git repo consists of code related to the classes and teaching in the undergraduate class 191B at UC Irvine.

Class website - <https://eee.uci.edu/12f/37110>

## Contents ##

### TDD ###

TDD stands for test driven development. This folder contains the code for a simple problem, which was implemented using unit tests and TDD. The following is the description of the problem.  

#### Problem Statement ####
WAP to take an integer and turn it into words, in English. 
The rules for this may change depending on the language, 
so if English is not your only language, you may like to 
try to repeat this exercise in another language.
So, if the integer is 1, the result will be "one". 
If the integer is 23 the result will be "twenty three" and so on.
- "Test Driven / First Development by Example", Code Project
<http://www.codeproject.com/Articles/18181/Test-Driven-First-Development-by-Example>

#### Good resources for a crash course in software testing ####
* JUnit Tutorial - <http://www.vogella.com/articles/JUnit/article.html>  
* Deciphering Testing Jargon <http://net.tutsplus.com/tutorials/php/deciphering-testing-jargon/>  
* "Jasmine" : JS Unit Testing framework - <http://pivotal.github.com/jasmine/>
* QUnit : JS Unit Testing framework - <http://qunitjs.com/>
* SimpleTest : PHP Unit Testing framework - <http://www.simpletest.org/>