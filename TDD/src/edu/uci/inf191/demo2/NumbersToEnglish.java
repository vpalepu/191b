package edu.uci.inf191.demo2;

public class NumbersToEnglish {
	public static String numbersToEnglish(int i) {
		if(i <= 19) {
			return get1to19(i);
		}
		return getTens(i/10) + (i%10 == 0 ? "" : " ") + get1to19(i%10);
	}
	
	private static String getTens(int i) {
		switch(i) {
		case 2: 
			return "twenty";
		case 3:
			return "thirty";
		default:
			return "forty";
		}
	}
	
	private static String get1to19(int i) {
		switch(i) {
		case 0:
			return "";
		case 1:
			return "one";
		case 2:
			return "two";
		case 3:
			return "three";
		case 4:
			return "four";
		case 5:
			return "five";
		case 6:
			return "six";
		case 7:
			return "seven";
		case 8:
			return "eight";
		case 9:
			return "nine";
		case 10:
			return "ten";
		case 11:
			return "eleven";
		case 12:
			return "twelve";
		case 13:
			return "thirteen";
		case 14:
			return "fourteen";
		case 15:
			return "fifteen";
		case 16:
			return "sixteen";
		case 17:
			return "seventeen";
		case 18:
			return "eighteen";
		default:
			return "nineteen";
		}
	}
}
