package edu.uci.inf191.demo2;

import static org.junit.Assert.*;

import org.junit.Test;

public class NumbersToEnglishTest {

	@Test
	public void NumbersToEnglishShouldReturnOne() {
		String actual = NumbersToEnglish.numbersToEnglish(1);
		assertEquals("one", actual);
	}
	
	@Test
	public void NumbersToEnglishShouldReturnTwo() {
		String actual = NumbersToEnglish.numbersToEnglish(2);
		assertEquals("two", actual);
	}
	
	@Test
	public void NumbersToEnglishShouldReturnTen() {
		String actual = NumbersToEnglish.numbersToEnglish(10);
		assertEquals("ten", actual);
	}
	
	@Test
	public void NumbersToEnglishShouldReturnEleven() {
		String actual = NumbersToEnglish.numbersToEnglish(11);
		assertEquals("eleven", actual);
	}
	
	@Test
	public void NumbersToEnglishShouldReturnNineTeen() {
		String actual = NumbersToEnglish.numbersToEnglish(19);
		assertEquals("nineteen", actual);
	}
	
	@Test
	public void NumbersToEnglishShouldReturnTwenty() {
		String actual = NumbersToEnglish.numbersToEnglish(20);
		assertEquals("twenty", actual);
	}
	
	@Test
	public void NumbersToEnglishShouldReturnTwentyOne() {
		String actual = NumbersToEnglish.numbersToEnglish(21);
		assertEquals("twenty one", actual);
	}
	
	@Test
	public void NumbersToEnglishShouldReturnTwentyNine() {
		String actual = NumbersToEnglish.numbersToEnglish(29);
		assertEquals("twenty nine", actual);
	}
	
	@Test
	public void NumbersToEnglishShouldReturnThirty() {
		String actual = NumbersToEnglish.numbersToEnglish(30);
		assertEquals("thirty", actual);
	}
	
	@Test
	public void NumbersToEnglishShouldReturnThirtyFive() {
		String actual = NumbersToEnglish.numbersToEnglish(35);
		assertEquals("thirty five", actual);
	}
	
	@Test
	public void NumbersToEnglishShouldReturnThirtyNine() {
		String actual = NumbersToEnglish.numbersToEnglish(39);
		assertEquals("thirty nine", actual);
	}
	
	@Test
	public void NumbersToEnglishShouldReturnForty() {
		String actual = NumbersToEnglish.numbersToEnglish(40);
		assertEquals("forty", actual);
	}
}

