package edu.uci.inf191.demo5;

import org.junit.Assert;
import org.junit.Ignore;
import org.junit.Test;

public class NumbersToEnglishTest {
	
	@Test
	public void testShouldReturnOneFor1() {
		String result = NumbersToEnglish.translateToEnglish(1);
		Assert.assertEquals("one", result);
	}
	
	@Ignore
	@Test
	public void testShouldReturnTwoFor2() {
		String result = NumbersToEnglish.translateToEnglish(2);
		Assert.assertEquals("two", result);
	}
	
	@Test
	public void testShouldReturnThreeFor3() {
		String result = NumbersToEnglish.translateToEnglish(3);
		Assert.assertEquals("three", result);
	}
	
	@Test
	public void testShouldReturnEightFor8() {
		String result = NumbersToEnglish.translateToEnglish(8);
		Assert.assertEquals("eight", result);
	}
	
	@Test
	public void testShouldReturnTenFor10() {
		String result = NumbersToEnglish.translateToEnglish(10);
		Assert.assertEquals("ten", result);
	}
	
	@Test
	public void testShouldReturnNineteenFor19() {
		String result = NumbersToEnglish.translateToEnglish(19);
		Assert.assertEquals("nineteen", result);
	}
	
	@Test
	public void testShouldReturnTwelveFor12() {
		String result = NumbersToEnglish.translateToEnglish(12);
		Assert.assertEquals("twelve", result);
	}
	
	@Test
	public void testShouldReturnTwentyEightFor28() {
		String result = NumbersToEnglish.translateToEnglish(28);
		Assert.assertEquals("twenty eight", result);
	}
	
	@Test
	public void testShouldReturnTwentyFor20() {
		String result = NumbersToEnglish.translateToEnglish(20);
		Assert.assertEquals("twenty", result);
	}
	
	@Test
	public void testShouldReturnMinusTwentyForMinus20() {
		String result = NumbersToEnglish.translateToEnglish(-20);
		Assert.assertEquals("minus twenty", result);
	}
	
	@Test
	public void testShouldReturnZeroForMinus0() {
		String result = NumbersToEnglish.translateToEnglish(0);
		Assert.assertEquals("zero", result);
	}
	

}
