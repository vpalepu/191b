package edu.uci.inf191.demo5;

public class NumbersToEnglish {
	
	public static String translateToEnglish(int number) {
		if(number > 0) {
			return translate1to29ToEnglish(number);
		}
		if(number == 0) return "zero";
		number = number * -1;
		return "minus " + translate1to29ToEnglish(number);
	}
	
	private static String translate1to29ToEnglish(int number) {
		String translation = null;
		
		if(number <= 19) {
			return translate1to19ToEnglish(number);
		}
	
		
		translation = "twenty";
		
		int units_digit = number % 10;
		
		if(units_digit != 0) {
			translation = translation + " " + translate1to19ToEnglish(units_digit);
		}
		
		return translation;
	}

	private static String translate1to19ToEnglish(int number) {
		switch(number) {
			case 1:
				return "one";
			case 2:
				return "two";
			case 3:
				return "three";
			case 4:
				return "four";
			case 5:
				return "five";
			case 6:
				return "six";
			case 7:
				return "seven";
			case 8:
				return "eight";
			case 9:
				return "nine";
			case 10:
				return "ten";
			case 11:
				return "eleven";
			case 12:
				return "twelve";
			case 13:
				return "thirteen";
			case 14:
				return "fourteen";
			case 15:
				return "fifteen";
			case 16:
				return "sixteen";
			case 17:
				return "seventeen";
			case 18:
				return "eighteen";
			default:
				return "nineteen";
		}
	}
	
}
