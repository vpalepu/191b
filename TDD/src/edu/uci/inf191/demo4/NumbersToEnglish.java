package edu.uci.inf191.demo4;

public class NumbersToEnglish {
	
	public static String translateToEnglish(int i) {
		String translation = null;
		if(i <= 99) {
			return translate1to99ToEnglish(i);
		}
		
		translation = "hundred";
		
		int remainder = i % 100;
		
		if(remainder != 0) {
			translation += " and " + translate1to99ToEnglish(remainder);
		}
		
		return translation;
	}
	
	private static String translate1to99ToEnglish(int i) {
		
		String translation = null;
		
		if(i <= 19) {
			return translate1to19ToEnglish(i);
		}
		
		int units = i % 10;
		int tens = i / 10;
		
		switch(tens) {
			case 2:
				translation = "twenty";
				break;
			case 3:
				translation = "thirty";
				break;
			case 4:
				translation = "forty";
				break;
			case 5:
				translation = "fifty";
				break;
			case 6:
				translation = "sixty";
				break;
			case 7:
				translation = "seventy";
				break;
			case 8:
				translation = "eighty";
				break;
			default:
				translation = "ninety";
				
		}
		
		if(units == 0) {
			return translation;
		}
		
		return translation + " " + translate1to19ToEnglish(units);
	}
	
	private static String translate1to19ToEnglish(int i) {
		switch(i) {
			case 1:
				return "one";
			case 2:
				return "two";
			case 3:
				return "three";
			case 4:
				return "four";
			case 5:
				return "five";
			case 6:
				return "six";
			case 7:
				return "seven";
			case 8:
				return "eight";
			case 9:
				return "nine";
			case 10:
				return "ten";
			case 11:
				return "eleven";
			case 12:
				return "twelve";
			case 13:
				return "thirteen";
			case 14:
				return "fourteen";
			case 15:
				return "fifteen";
			case 16:
				return "sixteen";
			case 17:
				return "seventeen";
			case 18:
				return "eighteen";
			default:
				return "nineteen";
		}
	}

}
