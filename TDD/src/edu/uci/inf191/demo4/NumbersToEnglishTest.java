package edu.uci.inf191.demo4;

import org.junit.Assert;
import org.junit.Test;

public class NumbersToEnglishTest {
	
	@Test
	public void shouldReturnOneFor1() {
		String result = NumbersToEnglish.translateToEnglish(1);
		Assert.assertEquals("one", result);
	}
	
	@Test
	public void shouldReturnTwoFor2() {
		String result = NumbersToEnglish.translateToEnglish(2);
		Assert.assertEquals("two", result);
	}
	
	@Test
	public void shouldReturnThreeFor3() {
		String result = NumbersToEnglish.translateToEnglish(3);
		Assert.assertEquals("three", result);
	}
	
	@Test
	public void shouldReturnNineFor9() {
		String result = NumbersToEnglish.translateToEnglish(9);
		Assert.assertEquals("nine", result);
	}
	
	@Test
	public void shouldReturnTenFor10() {
		String result = NumbersToEnglish.translateToEnglish(10);
		Assert.assertEquals("ten", result);
	}
	
	@Test
	public void shouldReturnElevenFor11() {
		String result = NumbersToEnglish.translateToEnglish(11);
		Assert.assertEquals("eleven", result);
	}
	
	@Test
	public void shouldReturnTenFor12() {
		String result = NumbersToEnglish.translateToEnglish(12);
		Assert.assertEquals("twelve", result);
	}
	
	@Test
	public void shouldReturnTenFor19() {
		String result = NumbersToEnglish.translateToEnglish(19);
		Assert.assertEquals("nineteen", result);
	}
	
	@Test
	public void shouldReturnTwentyFor20() {
		String result = NumbersToEnglish.translateToEnglish(20);
		Assert.assertEquals("twenty", result);
	}
	
	@Test
	public void shouldReturnTwentyFor21() {
		String result = NumbersToEnglish.translateToEnglish(21);
		Assert.assertEquals("twenty one", result);
	}
	@Test
	public void shouldReturnTwentyFor22() {
		String result = NumbersToEnglish.translateToEnglish(22);
		Assert.assertEquals("twenty two", result);
	}
	
	@Test
	public void shouldReturnTwentyFor29() {
		String result = NumbersToEnglish.translateToEnglish(29);
		Assert.assertEquals("twenty nine", result);
	}
	
	@Test
	public void shouldReturnTwentyFor30() {
		String result = NumbersToEnglish.translateToEnglish(30);
		Assert.assertEquals("thirty", result);
	}
	
	@Test
	public void shouldReturnTwentyFor31() {
		String result = NumbersToEnglish.translateToEnglish(31);
		Assert.assertEquals("thirty one", result);
	}
	@Test
	public void shouldReturnTwentyFor32() {
		String result = NumbersToEnglish.translateToEnglish(32);
		Assert.assertEquals("thirty two", result);
	}
	
	@Test
	public void shouldReturnTwentyFor39() {
		String result = NumbersToEnglish.translateToEnglish(39);
		Assert.assertEquals("thirty nine", result);
	}
	
	@Test
	public void shouldReturnTwentyFor90() {
		String result = NumbersToEnglish.translateToEnglish(90);
		Assert.assertEquals("ninety", result);
	}
	
	@Test
	public void shouldReturnTwentyFor91() {
		String result = NumbersToEnglish.translateToEnglish(91);
		Assert.assertEquals("ninety one", result);
	}
	@Test
	public void shouldReturnTwentyFor92() {
		String result = NumbersToEnglish.translateToEnglish(92);
		Assert.assertEquals("ninety two", result);
	}
	
	@Test
	public void shouldReturnTwentyFor98() {
		String result = NumbersToEnglish.translateToEnglish(98);
		Assert.assertEquals("ninety eight", result);
	}
	
	@Test
	public void shouldReturnHundredFor100() {
		String result = NumbersToEnglish.translateToEnglish(100);
		Assert.assertEquals("hundred", result);
	}
	
	@Test
	public void shouldReturnHundredFor101() {
		String result = NumbersToEnglish.translateToEnglish(101);
		Assert.assertEquals("hundred and one", result);
	}
	
	@Test
	public void shouldReturnHundredFor123() {
		String result = NumbersToEnglish.translateToEnglish(123);
		Assert.assertEquals("hundred and twenty three", result);
	}
	
	@Test
	public void shouldReturnHundredFor113() {
		String result = NumbersToEnglish.translateToEnglish(113);
		Assert.assertEquals("hundred and thirteen", result);
	}
	
}
