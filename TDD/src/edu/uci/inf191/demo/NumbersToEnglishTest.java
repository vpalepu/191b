package edu.uci.inf191.demo;

import static org.junit.Assert.*;

import org.junit.Test;

public class NumbersToEnglishTest {

	@Test
	public void NumbersToEnglishShouldReturnOne() {
		String actual = NumbersToEnglish.numbersToEnglish(1);
		assertEquals("one", actual);
	}
	
	@Test
	public void NumbersToEnglishShouldReturnTwo() {
		String actual = NumbersToEnglish.numbersToEnglish(2);
		assertEquals("two", actual);
	}
	
	@Test
	public void NumbersToEnglishShouldReturnTwenty() {
		String actual = NumbersToEnglish.numbersToEnglish(20);
		assertEquals("twenty", actual);
	}
	
	@Test
	public void NumbersToEnglishShouldReturnTwentyoOne() {
		String actual = NumbersToEnglish.numbersToEnglish(21);
		assertEquals("twenty one", actual);
	}

}
