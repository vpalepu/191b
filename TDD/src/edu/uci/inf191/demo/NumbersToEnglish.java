package edu.uci.inf191.demo;

/*
* This is a dummy change
*/

/*
 * "Agile development methodologies, especially eXtreme Programming,
 * suggests that you do the simplest thing possible to get the thing working. "
 * 
 * WAP to take an integer and turn it into words, in English. 
 * The rules for this may change depending on the language, 
 * so if English is not your only language, you may like to 
 * try to repeat this exercise in another language.
 * So, if the integer is 1, the result will be "one". 
 * If the integer is 23 the result will be "twenty three" and so on.
 * - "Test Driven / First Development by Example", Code Project
 * http://www.codeproject.com/Articles/18181/Test-Driven-First-Development-by-Example
 * 
 * Good resources for a crash course in software testing - 
 * JUnit Tutorial - http://www.vogella.com/articles/JUnit/article.html
 * Deciphering Testing Jargon http://net.tutsplus.com/tutorials/php/deciphering-testing-jargon/
 * "Jasmine" : JS Unit Testing framework - http://pivotal.github.com/jasmine/
 */

public class NumbersToEnglish {

	public static String numbersToEnglish(int number) {
		if(number < 20) {
			return translate1to19(number);
		}
		else {
			if(number%20 == 0) {
				return "twenty";
			}
			return "twenty " + translate1to19(number%20);
		}
	}
	
	private static String translate1to19(int number) {
		switch(number) {
		case 1:
			return "one";
		case 2:
			return "two";
		case 3:
			return "three";
		case 4:
			return "four";
		case 5:
			return "five";
		case 6:
			return "six";
		case 7:
			return "seven";
		case 8:
			return "eight";
		case 9:
			return "nine";
		case 10:
			return "ten";
		case 11:
			return "eleven";
		case 12:
			return "twelve";
		case 13:
			return "thirteen";
		case 14:
			return "fourteen";
		case 15:
			return "fifteen";
		case 16:
			return "sixteen";
		case 17:
			return "seventeen";
		case 18:
			return "eighteen";
		default:
			return "nineteen";
		}
	}
}
