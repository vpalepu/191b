package edu.uci.inf191;

import static org.junit.Assert.*;

import org.junit.Test;

public class NumbersToStringTest {

	@Test
	public void NumberToEnglishShouldReturnOne() {
		String actual = NumbersToString.numbersToEnglish(1);
		assertEquals("Expected result one.", "one", actual);
	}
	
	@Test
	public void NumberToEnglishShouldReturnTwo() {
		String actual = NumbersToString.numbersToEnglish(2);
		assertEquals("Expected result two.", "two", actual);
	}
	
	@Test
	public void NumberToEnglishShouldReturnThree() {
		String actual = NumbersToString.numbersToEnglish(3);
		assertEquals("Expected result three.", "three", actual);
	}
	
	@Test
	public void NumberToEnglishShouldReturnTwentyOne() {
		String actual = NumbersToString.numbersToEnglish(21);
		assertEquals("Expected result Twenty One.", "twenty one", actual);
	}
	
	@Test
	public void NumberToEnglishShouldReturnTwentyNine() {
		String actual = NumbersToString.numbersToEnglish(29);
		assertEquals("Expected result Twenty One.", "twenty nine", actual);
	}
	
	@Test
	public void NumberToEnglishShouldReturnThirty() {
		String actual = NumbersToString.numbersToEnglish(30);
		assertEquals("Expected result Thirty.", "thirty", actual);
	}
	
	@Test
	public void NumberToEnglishShouldReturnThirtyOne() {
		String actual = NumbersToString.numbersToEnglish(31);
		assertEquals("Expected result Thirty One.", "thirty one", actual);
	}
	
	@Test
	public void NumberToEnglishShouldReturn99() {
		String actual = NumbersToString.numbersToEnglish(99);
		assertEquals("Expected result Ninety Nine.", "ninety nine", actual);
	}

}
