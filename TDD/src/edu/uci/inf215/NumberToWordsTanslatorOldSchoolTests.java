package edu.uci.inf215;

import static edu.uci.inf215.NumberTranslator.translator;
import static org.junit.Assert.*;

import org.junit.Test;

import edu.uci.inf215.NumberTranslator.Helper;

public class NumberToWordsTanslatorOldSchoolTests {

	@Test
	public void test0() {
		String translation = translator().translateToWords(1);
		assertEquals("Expected result one.", "one", translation);
	}

	@Test
	public void test1() {
		String translation = translator().translateToWords(2);
		assertEquals("Expected result two.", "two", translation);
	}
	
	@Test
	public void test2() {
		String translation = translator().translateToWords(3);
		assertEquals("Expected result three.", "three", translation);
	}
	
	@Test
	public void test3() {
		String translation = translator().translateToWords(19);
		assertEquals("Expected result nineteen.", "nineteen", translation);
	}
	
	@Test
	public void test4() {
		String translation = translator().translateToWords(21);
		assertEquals("Expected result twenty one.", "twenty one", translation);
	}
	
	@Test
	public void test5() {
		String translation = translator().translateToWords(30);
		assertEquals("Expected result thirty.", "thirty", translation);
	}
	
	@Test
	public void test6() {
		int number = 99;
		String translation = translator().translateToWords(number);
		assertEquals("Expected result ninety nine.", "ninety nine", translation);
	}
	
	@Test
  public void test7() {
    int number = 99;
    String translation = translator().translateToWords(number);
    assertEquals(2, Helper.wordCount(translation));
  }

}
