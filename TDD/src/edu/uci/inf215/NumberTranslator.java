package edu.uci.inf215;

import java.io.BufferedReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;
import java.util.Scanner;

public class NumberTranslator {
	
	private static NumberTranslator singleton = null;
	public static NumberTranslator translator() {
		if(singleton == null)
			singleton = new NumberTranslator();
		return singleton;
	}
	
	ArrayList<String> translateAnyIntegersToWords(Iterable<String> wordBag) {
    ArrayList<String> tranlatedWordBag = new ArrayList<>();
    for(String word : wordBag) {
      int number;
      try{
        number = Integer.parseInt(word);
      } catch(NumberFormatException ex) {
        tranlatedWordBag.add(word);
        continue;
      }
      tranlatedWordBag.add(translateToWords(number));
    }
    return tranlatedWordBag;
  }
	
	ArrayList<String> translateAnyIntegersToWords(BufferedReader reader) {
    ArrayList<String> tranlations = new ArrayList<>();
    String line = null;
    try {
      while((line = reader.readLine()) != null) {
        ArrayList<String> interm = translateAnyIntegersToWords(Helper.split(line));
        tranlations.addAll(interm);
      }
      reader.close();
    } catch (IOException e) {
      e.printStackTrace();
    }
    return tranlations;
  }
	
	ArrayList<String> translateToWords(ArrayList<Integer> numbers) {
    ArrayList<String> tranlations = new ArrayList<>();
    for(int number : numbers) {
      tranlations.add(translateToWords(number));
    }
    return tranlations;
  }
	
	public String translateToWords(int number) {
	  if(number == 0) 
      return "zero";
	  
	  if(number > 0) {
      return translate1to99(number);
    }
    
    return "minus " + translate1to99(number * -1);
  }
	
	public String translate1to99(int number) {
		if(number < 20) {
			return translate1To19(number);
		}

		String translation = null;
		
		int tens = number / 10;
		int units = number % 10;

		switch(tens) {
		case 2:
			translation = "twenty";
			break;
		case 3:
			translation = "thirty";
			break;
		case 4:
			translation = "forty";
			break;
		case 5:
			translation = "fifty";
			break;
		case 6:
			translation = "sixty";
			break;
		case 7:
			translation = "seventy";
			break;
		case 8:
			translation = "eighty";
			break;
		default:
			translation = "ninety";
			break;
		}

		if(units != 0) {
			translation = translation + " " + translate1To19(units);
		}
		return translation;
	}

	public String translate1To19(int number) {
		String string = null;
		switch(number) {
		case 1:
			string = "one";
			break;
		case 2:
			string = "two";
			break;
		case 3:
			string = "three";
			break;
		case 4:
			string = "four";
			break;
		case 5:
			string = "five";
			break;
		case 6:
			string = "six";
			break;
		case 7:
			string = "seven";
			break;
		case 8:
			string = "eight";
			break;
		case 9:
			string = "nine";
			break;
		case 10:
			string = "ten";
			break;
		case 11:
			string = "eleven";
			break;
		case 12:
			string = "twelve";
			break;
		case 13:
			string = "thirteen";
			break;
		case 14:
			string = "fourteen";
			break;
		case 15:
			string = "fifteen";
			break;
		case 16:
			string = "sixteen";
			break;
		case 17:
			string = "seventeen";
			break;
		case 18:
			string = "eighteen";
			break;
		case 19:
			string = "nineteen";
			break;
		default:
			break;
		}
		return string;
	}
	
	public static class Helper {
	  public static int wordCount(String translation) {
	    String[] words = translation.split("\\s+");
	    return words.length;
	  }
	  
	  public static ArrayList<String> split(String line) {
	    String[] words = line.split("\\s+");
	    return new ArrayList<>(Arrays.asList(words));
	  }
	}
}
