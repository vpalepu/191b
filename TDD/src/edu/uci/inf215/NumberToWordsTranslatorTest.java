package edu.uci.inf215;

import static org.junit.Assert.*;

import org.junit.Test;

import static edu.uci.inf215.NumberTranslator.translator;
import static edu.uci.inf215.NumberTranslator.Helper.wordCount;


/**
 * User Story:
 *  As a *Student*,
 *  I want to *Translate mathematical numerals into words in English*,
 *  so that *my English passages are grammatically correct when using numbers.*
 * @author vijay
 *
 */
public class NumberToWordsTranslatorTest {

	/*
	 * Scenario: Number is 1
	 *	Given number is 1
	 *	When translated to words,
	 *	Then translation is “one”.
	 */
	@Test
	public void translationShouldReturnOne() {
		// Given
		int number = 1;
		// When
		String translation = translator().translateToWords(number);
		// Then
		assertEquals("Expected result one.", "one", translation);
	}

	/*
	 * Scenario: Number is 2
	 *	Given number is 2
	 *	When translated to words,
	 *	Then translation is “two”.
	 */
	@Test
	public void translationShouldReturnTwo() {
		// Given
		int number = 2;
		// When
		String translation = translator().translateToWords(number);
		// Then
		assertEquals("Expected result two.", "two", translation);
	}
	
	/*
	 * Scenario: Number is 3
	 *	Given number is 3
	 *	When translated to words,
	 *	Then translation is “three”.
	 */
	@Test
	public void translationShouldReturnThree() {
		// Given
		int number = 2;
		// When
		String translation = translator().translateToWords(number);
		// Then
		assertEquals("Expected result two.", "two", translation);
	}
	
	/*
	 * Scenario: Number is 19
	 *	Given number is 3
	 *	When translated to words,
	 *	Then translation is “nineteen”.
	 */
	@Test
	public void translationShouldReturnNineteen() {
		// Given
		int number = 19;
		// When
		String translation = translator().translateToWords(number);
		// Then
		assertEquals("Expected result nineteen.", "nineteen", translation);
	}
	
	/*
	 * Scenario: Number is 21
	 *	Given number is 21
	 *	When translated to words,
	 *	Then translation is “twenty one”.
	 */
	@Test
	public void translationShouldReturnTwentyOne() {
		// Given
		int number = 21;
		// When
		String translation = translator().translateToWords(number);
		// Then
		assertEquals("Expected result twenty one.", "twenty one", translation);
	}
	
	/*
	 * Scenario: Number is 30
	 *	Given number is 30
	 *	When translated to words,
	 *	Then translation is “thirty”.
	 */
	@Test
	public void translationShouldReturnThirty() {
		// Given
		int number = 30;
		// When
		String translation = translator().translateToWords(number);
		// Then
		assertEquals("Expected result thirty.", "thirty", translation);
	}
	
	/*
	 * Scenario: Number is 99
	 *	Given number is 99
	 *	When translated to words,
	 *	Then translation is “ninety nine”.
	 */
	@Test
	public void translationShouldReturnNinetyNine() {
		// Given
		int number = 99;
		// When
		String translation = translator().translateToWords(number);
		// Then
		assertEquals("Expected result ninety nine.", "ninety nine", translation);
	}
	
	@Test
  public void translationFor99ShouldContainTwoWords() {
	  // Given
    int number = 99;
    // When
    String translation = translator().translateToWords(number);
    // Then
    assertEquals(2, wordCount(translation));
  }

}
