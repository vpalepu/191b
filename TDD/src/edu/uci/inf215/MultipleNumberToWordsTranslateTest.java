package edu.uci.inf215;

import static org.junit.Assert.*;
import static edu.uci.inf215.NumberTranslator.translator;
import static org.mockito.Mockito.*;
import static org.mockito.BDDMockito.*;

import java.io.BufferedReader;
import java.io.IOException;
import java.util.ArrayList;
import org.junit.Test;

public class MultipleNumberToWordsTranslateTest {

  @Test
  public void shouldTranslateTwoIntegers_oldschool() {
    //given
    ArrayList<Integer> numbers = new ArrayList<>();
    numbers.add(1);
    numbers.add(2);
    
    //when
    ArrayList<String> translations = translator().translateToWords(numbers);
    
    //then
    assertEquals("one", translations.get(0));
    assertEquals("two", translations.get(1));
  }
  
  @Test
  public void shouldInvokeTranslateToWordsTwice() { // SPIES!
    //given
    NumberTranslator translator = spy(translator());
    ArrayList<Integer> numbers = new ArrayList<>();
    numbers.add(1);
    numbers.add(2);
    
    //when
    translator.translateToWords(numbers);
    
    //then
    verify(translator).translateToWords(1);
    verify(translator).translateToWords(2);
    verify(translator, times(3)).translateToWords(anyInt());
  }
  
  @Test
  public void shouldInvokeTranslateToWordsOnceAndNeverInvokeTranslate1to19() { // SPIES -- can skip!
    //given
    NumberTranslator translator = spy(translator());
    ArrayList<Integer> numbers = new ArrayList<>();
    numbers.add(20);
    
    //when
    translator.translateToWords(numbers);
    
    //then
    verify(translator).translateToWords(anyInt());
    verify(translator, never()).translate1To19(anyInt());
  }
  
  @Test
  public void shouldInvokeTranslateToWordsThrice() {
    //given
    NumberTranslator translator = spy(translator()); 
    ArrayList<String> numbers = new ArrayList<>();
    numbers.add("1");
    numbers.add("2");
    numbers.add("3");
    
    //when
    translator.translateAnyIntegersToWords(numbers);
    
    //then
    verify(translator, times(3)).translateToWords(anyInt());
  }
  
  @Test
  public void shouldInvokeReadLineTwice() throws IOException {
//    BufferedReader reader = new BufferedReader(new FileReader(new File("")));

    // given
    BufferedReader reader = mock(BufferedReader.class);
    when(reader.readLine()).thenReturn("1 to three.")
                           .thenReturn(null); // stub
    NumberTranslator translator = translator();
    
    //when
    translator.translateAnyIntegersToWords(reader);
    
    //then
    verify(reader, times(2)).readLine();
  }
  
  @Test
  public void shouldInvokeTranslateToWordsOnce_bddStyle() throws IOException {
    //given
    BufferedReader reader = mock(BufferedReader.class);
    given(reader.readLine()).willReturn("1 to three.")
                            .willReturn(null);
    NumberTranslator translator = spy(translator()); 
    
    //when
    translator.translateAnyIntegersToWords(reader);
    
    //then
    verify(translator, times(1)).translateToWords(anyInt());
  }

}
