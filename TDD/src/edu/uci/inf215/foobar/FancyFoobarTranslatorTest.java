package edu.uci.inf215.foobar;

import static org.junit.Assert.*;
import static org.mockito.Mockito.*;
import static org.mockito.BDDMockito.*;

import org.junit.Test;

import edu.uci.inf215.NumberTranslator;

public class FancyFoobarTranslatorTest {

  @Test
  public void fancyTranslation_Of_35_ShouldBe_Bar() {
    //given
    FooBarTranslator translator = FooBarTranslator.translator();
    NumberTranslator numberTranslator = NumberTranslator.translator();
    int number = 35;
    
    //when
    String translation = translator.fancyTranslate(number, numberTranslator);
    
    //then
    assertEquals("bar", translation);
  }
  
  @Test
  public void fancyTranslation_Of_19_ShouldBe_Nineteen() { // regular test
    //given
    FooBarTranslator translator = FooBarTranslator.translator();
    NumberTranslator numberTranslator = NumberTranslator.translator();
    int number = 19;
    
    //when
    String translation = translator.fancyTranslate(number, numberTranslator);
    
    //then
    assertEquals("nineteen", translation);
  }
  
  @Test
  public void fancyTranslation_Should_Inovke_NumberTranslation() { // MOCKING!
    //given
    FooBarTranslator foobarTranslator = FooBarTranslator.translator();
    NumberTranslator numberTranslator = mock(NumberTranslator.class);
    int number = 19;
    
    //when
    //String translation = 
        foobarTranslator.fancyTranslate(number, numberTranslator);
    
    //then
//    assertEquals("nineteen", translation); // will it work?
    verify(numberTranslator).translateToWords(number); // will it work?
//    verify(numberTranslator).translate1To19(number); // will it work?
  }
  
  @Test
  public void fancyTranslation_Should_Inovke_NumberTranslation_And_Return_Nineteen() { // STUBBING with MOCKING!
    //given
    FooBarTranslator translator = FooBarTranslator.translator(); //real
    NumberTranslator numberTranslator = mock(NumberTranslator.class); //mock
    when(numberTranslator.translateToWords(19)).thenReturn("nineteen"); //stub
    int number = 19;
    
    //when
    String translation = translator.fancyTranslate(number, numberTranslator);
    
    //then
    verify(numberTranslator, times(1)).translateToWords(number); // will it work?
    assertEquals("nineteen", translation); // will it work?
  }
  
  @Test
  public void fancyTranslation_Should_Inovke_NumberTranslation_ExactlyOnce() { // More MOCKING!
    //given
    FooBarTranslator translator = FooBarTranslator.translator();
    NumberTranslator numberTranslator = mock(NumberTranslator.class);
    int number = 19;
    
    //when
    translator.fancyTranslate(number, numberTranslator);
    
    //then
    verify(numberTranslator, times(1)).translateToWords(number);
  }
  
  @Test
  public void fancyTranslation_Should_Inovke_NumberTranslation_Never() { // More MOCKING!
    //given
    FooBarTranslator translator = FooBarTranslator.translator();
    NumberTranslator numberTranslator = mock(NumberTranslator.class);
    int number = 35;
    
    //when
    translator.fancyTranslate(number, numberTranslator);
    
    //then
    verify(numberTranslator, never()).translateToWords(number);
  }

}
