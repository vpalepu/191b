package edu.uci.inf215.foobar;

import edu.uci.inf215.NumberTranslator;

public class FooBarTranslator {
  
  private static FooBarTranslator singleton = null;
  public static FooBarTranslator translator() {
    if(singleton == null)
      singleton = new FooBarTranslator();
    return singleton;
  }
  
  public String translate(int number) {
    String translation = "";
    
    if(number % 3 == 0) {
      translation += "foo";
    }
    
    if(number % 5 == 0) {
      translation += "bar";
    }
    
    if(translation.isEmpty()) {
      return String.valueOf(number);
    }

    return translation;
  }
  
  public String fancyTranslate(int number, NumberTranslator numberTranslator) {
    String tranlation = translate(number);
    if(tranlation.contains("foo") || tranlation.contains("bar")) {
      return tranlation;
    }
    
    number = Integer.parseInt(tranlation);
    String fancyTranslation = numberTranslator.translateToWords(number);
    return fancyTranslation;
  }

}
