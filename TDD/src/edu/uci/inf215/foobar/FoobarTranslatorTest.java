package edu.uci.inf215.foobar;

import static org.junit.Assert.*;

import org.junit.Test;

public class FoobarTranslatorTest {

  @Test
  public void shouldTranslateNumber1to1() {
    //given
    FooBarTranslator translator = FooBarTranslator.translator();
    int number = 1;
    
    //when
    String translation = translator.translate(number);
    
    //then
    assertEquals("1", translation);
  }
  
  @Test
  public void shouldTranslateNumber2to2() {
    //given
    FooBarTranslator translator = FooBarTranslator.translator();
    int number = 2;
    
    //when
    String translation = translator.translate(number);
    
    //then
    assertEquals("2", translation);
  }
  
  @Test
  public void shouldTranslateNumber3toFoo() {
    //given
    FooBarTranslator translator = FooBarTranslator.translator();
    int number = 3;
    
    //when
    String translation = translator.translate(number);
    
    //then
    assertEquals("foo", translation);
  }
  
  @Test
  public void shouldTranslateNumber5toBar() {
    //given
    FooBarTranslator translator = FooBarTranslator.translator();
    int number = 5;
    
    //when
    String translation = translator.translate(number);
    
    //then
    assertEquals("bar", translation);
  }
  
  @Test
  public void shouldTranslateNumber9toFoo() {
    //given
    FooBarTranslator translator = FooBarTranslator.translator();
    int number = 9;
    
    //when
    String translation = translator.translate(number);
    
    //then
    assertEquals("foo", translation);
  }
  
  @Test
  public void shouldTranslateNumber10toBar() {
    //given
    FooBarTranslator translator = FooBarTranslator.translator();
    int number = 10;
    
    //when
    String translation = translator.translate(number);
    
    //then
    assertEquals("bar", translation);
  }
  
  @Test
  public void shouldTranslateNumber15toFooBar() {
    //given
    FooBarTranslator translator = FooBarTranslator.translator();
    int number = 15;
    
    //when
    String translation = translator.translate(number);
    
    //then
    assertEquals("foobar", translation);
  }
  
  @Test
  public void shouldTranslateNumber25toBar() {
    //given
    FooBarTranslator translator = FooBarTranslator.translator();
    int number = 25;
    
    //when
    String translation = translator.translate(number);
    
    //then
    assertEquals("bar", translation);
  }
  
  @Test
  public void shouldTranslateNumber30toFooBar() {
    //given
    FooBarTranslator translator = FooBarTranslator.translator();
    int number = 30;
    
    //when
    String translation = translator.translate(number);
    
    //then
    assertEquals("foobar", translation);
  }

}
