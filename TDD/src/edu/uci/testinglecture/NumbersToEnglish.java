package edu.uci.testinglecture;

public class NumbersToEnglish {
	public static String toEnglishWords(int number) {
		if(number == 2) return "two";
		return "one";
	}
}
