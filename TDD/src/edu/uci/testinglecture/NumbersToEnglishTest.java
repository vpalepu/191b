package edu.uci.testinglecture;

import static org.junit.Assert.*;

import org.junit.Ignore;
import org.junit.Test;

public class NumbersToEnglishTest {

	@Test
	@Ignore
	public void test() {
		fail("Not yet implemented");
	}
	
	@Test
	public void shouldReturnOneFor1() {
		String result = NumbersToEnglish.toEnglishWords(1);
		assertEquals("one", result);
	}
	
	@Test
	public void shouldReturnTwoFor2() {
		String result = NumbersToEnglish.toEnglishWords(2);
		assertEquals("two", result);
	}

}
