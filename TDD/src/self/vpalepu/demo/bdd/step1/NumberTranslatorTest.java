package self.vpalepu.demo.bdd.step1;

import static org.junit.Assert.*;

import org.junit.Test;

import self.vpalepu.demo.bdd.step1.NumberTranslator;

public class NumberTranslatorTest {

	/*
	 * Scenario: Number is 1
	 *	Given number is 1
	 *	When translated to words,
	 *	Then translation is “one”.
	 */
	@Test
	public void translationShouldReturnOne() {
		// Given number is 1
		int number = 1;
		// When translated to words
		String translation = NumberTranslator.translateToEnglish(number);
		// Then translation is “one”
		assertEquals("Expected result one.", "one", translation);
	}

	/*
	 * Scenario: Number is 2
	 *	Given number is 2
	 *	When translated to words,
	 *	Then translation is “two”.
	 */
	@Test
	public void translationShouldReturnTwo() {
		// Given number is 2
		int number = 2;
		// When translated to words
		String translation = NumberTranslator.translateToEnglish(number);
		// Then translation is “two”.
		assertEquals("Expected result two.", "two", translation);
	}
	
	

}
