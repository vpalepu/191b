package self.vpalepu.demo.bdd.step1;

/*
 * User Story: Translate Numbers from Numerals to Words in English
 *	_As a_ Newspaper Editor,
 *	_I want to_ edit newspaper articles to translate numerals into actual words,
 *	_so that_ I can have the satisfaction of following some esoteric rule of English Grammar.
 */

public class NumberTranslator {
	
//	// Step: 0
//	public static String translateToEnglish(int number) {
//		return "one";
//	}
	
	// Step: 1
	public static String translateToEnglish(int number) {
//		if(number == 2) {
//			return "two";
//		} // else if (number == 2)
		return "one";
	}

}
