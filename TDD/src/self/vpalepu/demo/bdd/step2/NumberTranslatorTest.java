package self.vpalepu.demo.bdd.step2;

import static org.junit.Assert.*;

import org.junit.Test;

import self.vpalepu.demo.bdd.step2.NumberTranslator;

public class NumberTranslatorTest {

	/*
	 * Scenario: Number is 1
	 *	Given number is 1
	 *	When translated to words,
	 *	Then translation is “one”.
	 */
	@Test
	public void translationShouldReturnOne() {
		// Given
		int number = 1;
		// When
		String translation = NumberTranslator.translateToEnglish(number);
		// Then
		assertEquals("Expected result one.", "one", translation);
	}

	/*
	 * Scenario: Number is 2
	 *	Given number is 2
	 *	When translated to words,
	 *	Then translation is “two”.
	 */
	@Test
	public void translationShouldReturnTwo() {
		// Given
		int number = 2;
		// When
		String translation = NumberTranslator.translateToEnglish(number);
		// Then
		assertEquals("Expected result two.", "two", translation);
	}
	
	/*
	 * Scenario: Number is 3
	 *	Given number is 3
	 *	When translated to words,
	 *	Then translation is “three”.
	 */
	@Test
	public void translationShouldReturnThree() {
		// Given
		int number = 3;
		// When
		String translation = NumberTranslator.translateToEnglish(number);
		// Then
		assertEquals("Expected result three.", "three", translation);
	}
	
	/*
	 * Scenario: Number is 19
	 *	Given number is 19
	 *	When translated to words,
	 *	Then translation is “nineteen”.
	 */
	@Test
	public void translationShouldReturnNineteen() {
		// Given
		int number = 19;
		// When
		String translation = NumberTranslator.translateToEnglish(number);
		// Then
		assertEquals("Expected result nineteen.", "nineteen", translation);
	}
	
	/*
	 * Scenario: Number between 13 and 19
	 *	Given number is less than equal to 13
	 *	and number greater than equal to 19
	 *	When translated to words,
	 *	Then translation ends with “teen”
	 *	and translation has one word.
	 */
	@Test
	public void translation13ShouldEndWithTeen() {
		// Given
		int number = 13;
		// When
		String translation = NumberTranslator.translateToEnglish(number);
		// Then
		assertTrue("Expected to end with 'teen'.", translation.endsWith("teen"));
	}

}
