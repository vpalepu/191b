package self.vpalepu.demo.bdd.step2;

/*
 * User Story: Translate Numbers from Numerals to Words in English
 *	_As a_ Newspaper Editor,
 *	_I want to_ edit newspaper articles to translate numerals into actual words,
 *	_so that_ I can have the satisfaction of following some esoteric rule of English Grammar.
 */

public class NumberTranslator {
	
	// Step: 1
//	public static String translateToEnglish(int number) {
//		if(number == 1) {
//			return "one";
//		}
//		return "two";
//	}
	
	// Step 2:
	public static String translateToEnglish(int number) {
		String string = null;
		switch(number) {
		case 1:
			string = "one";
			break;
		case 2:
			string = "two";
			break;
		case 3:
			string = "three";
			break;
		case 4:
			string = "four";
			break;
		case 5:
			string = "five";
			break;
		case 6:
			string = "six";
			break;
		case 7:
			string = "seven";
			break;
		case 8:
			string = "eight";
			break;
		case 9:
			string = "nine";
			break;
		case 10:
			string = "ten";
			break;
		case 11:
			string = "eleven";
			break;
		case 12:
			string = "twelve";
			break;
		case 13:
			string = "thirteen";
			break;
		case 14:
			string = "fourteen";
			break;
		case 15:
			string = "fifteen";
			break;
		case 16:
			string = "sixteen";
			break;
		case 17:
			string = "seventeen";
			break;
		case 18:
			string = "eighteen";
			break;
		case 19:
			string = "nineteen";
			break;
		default:
			break;
		}
		return string;
	}
	
	// Step 2b:
//	public static int countWords(String translation) {
//		String[] words = translation.split(" ");
//		return words.length;
//	}

}
