package self.vpalepu.demo.bdd.step3;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.util.Arrays;
import java.util.Collection;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.junit.runners.Parameterized.Parameters;

@RunWith(Parameterized.class)
public class NumberTranslatorNinetyTest {
	/*
	 * Scenario: Two digit number starts with 9
	 *	Given number has two digits
	 *	and number starts with 9
	 *	When translated to words,
	 *	Then translation starts with “ninety”.
	 */
	
	 // Given number has two digits
	 // and number starts with 9
	@Parameters(name = "{0}={1}")
    public static Collection<Object[]> data() {
        return Arrays.asList(new Object[][] {
                 { 90, "ninety" }, 
                 { 91, "ninety one" }, 
                 { 92, "ninety two" }, 
                 { 93, "ninety three" }, 
                 { 94, "ninety four" }, 
                 { 95, "ninety five" },
                 { 96, "ninety six" },
                 { 97, "ninety seven" },
                 { 98, "ninety eight" },
                 { 99, "ninety nine" },
           });
    }

    private int number;

    private String expectedTranslation;

    public NumberTranslatorNinetyTest(int input, String expected) {
        number = input;
        expectedTranslation = expected;
    }
    
    @Test
    public void translationShouldStartWithNinety() {
    	// When
    	String translation = NumberTranslator.translateToEnglish(number);
    	// Then translation starts with “ninety”.
    	assertTrue("Expected to start with 'ninety'.", 
    			translation.startsWith("ninety"));
    }
    
    @Test
    public void translationShouldBeAsExpected() {
    	// When
    	String translation = NumberTranslator.translateToEnglish(number);
    	// Then translation is as expected.
        assertEquals(expectedTranslation, translation);
    }
}
