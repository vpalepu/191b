package self.vpalepu.demo.bdd.step3;

import static org.junit.Assert.*;

import org.junit.Test;

public class NumberTranslatorTest {
	
	/*
	 * Scenario: Number is 1
	 *	Given number is 1
	 *	When translated to words,
	 *	Then translation is “one”.
	 */
	@Test
	public void translationShouldReturnOne() {
		// Given
		int number = 1;
		// When
		String translation = NumberTranslator.translateToEnglish(number);
		// Then
		assertEquals("Expected result one.", "one", translation);
	}

	/*
	 * Scenario: Number is 2
	 *	Given number is 2
	 *	When translated to words,
	 *	Then translation is “two”.
	 */
	@Test
	public void translationShouldReturnTwo() {
		// Given
		int number = 2;
		// When
		String translation = NumberTranslator.translateToEnglish(number);
		// Then
		assertEquals("Expected result two.", "two", translation);
	}
	
	/*
	 * Scenario: Number is 3
	 *	Given number is 3
	 *	When translated to words,
	 *	Then translation is “three”.
	 */
	@Test
	public void translationShouldReturnThree() {
		// Given
		int number = 2;
		// When
		String translation = NumberTranslator.translateToEnglish(number);
		// Then
		assertEquals("Expected result two.", "two", translation);
	}
	
	/*
	 * Scenario: Number is 19
	 *	Given number is 3
	 *	When translated to words,
	 *	Then translation is “nineteen”.
	 */
	@Test
	public void translationShouldReturnNineteen() {
		// Given
		int number = 19;
		// When
		String translation = NumberTranslator.translateToEnglish(number);
		// Then
		assertEquals("Expected result nineteen.", "nineteen", translation);
	}
	
	
	/*
	 * Scenario: Number is 21
	 *	Given number is 21
	 *	When translated to words,
	 *	Then translation is “twenty one”.
	 */
	@Test
	public void translationShouldReturnTwentyOne() {
		// Given
		int number = 21;
		// When
		String translation = NumberTranslator.translateToEnglish(number);
		// Then
		assertEquals("Expected result twenty one.", "twenty one", translation);
	}
	
	/*
	 * Scenario: Number is 30
	 *	Given number is 30
	 *	When translated to words,
	 *	Then translation is “thirty”.
	 */
	@Test
	public void translationShouldReturnThirty() {
		// Given
		int number = 30;
		// When
		String translation = NumberTranslator.translateToEnglish(number);
		// Then
		assertEquals("Expected result thirty.", "thirty", translation);
	}
	
	/*
	 * Scenario: Number is 99
	 *	Given number is 99
	 *	When translated to words,
	 *	Then translation is “ninety nine”.
	 */
	@Test
	public void translationShouldReturnNinetyNine() {
		// Given
		int number = 99;
		// When
		String translation = NumberTranslator.translateToEnglish(number);
		// Then
		assertEquals("Expected result ninety nine.", "ninety nine", translation);
	}
	
	// TODO convert to parameterized tests
	
	/*
	 * Scenario: Number between 13 and 19
	 *	Given number is less than equal to 13
	 *	and number greater than equal to 19
	 *	When translated to words,
	 *	Then translation ends with “teen”
	 *	and translation has one word.
	 */
	@Test
	public void translation13ShouldEndWithTeen() {
		// Given
		int number = 13;
		// When
		String translation = NumberTranslator.translateToEnglish(number);
		// Then
		assertTrue("Expected to end with 'teen'.", translation.endsWith("teen"));
	}
	
	/*
	 * Scenario: Number between 13 and 19
	 *	Given number is less than equal to 13
	 *	and number greater than equal to 19
	 *	When translated to words,
	 *	Then translation ends with “teen”
	 *	and translation has one word.
	 */
	@Test
	public void translation15ShouldEndWithTeen() {
		// Given
		int number = 15;
		// When
		String translation = NumberTranslator.translateToEnglish(number);
		// Then
		assertTrue("Expected to end with 'teen'.", translation.endsWith("teen"));
	}
	

}
