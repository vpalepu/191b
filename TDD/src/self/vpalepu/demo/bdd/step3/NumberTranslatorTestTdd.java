package self.vpalepu.demo.bdd.step3;

import static org.junit.Assert.*;

import org.junit.Test;

/*
 * User Story: Translate Numbers from Numerals to Words in English
 *	_As a_ Newspaper Editor,
 *	_I want to_ edit newspaper articles to translate numerals into actual words,
 *	_so that_ I can have the satisfaction of following some esoteric rule of English Grammar.
 */

public class NumberTranslatorTestTdd {

	@Test
	public void NumberToEnglishShouldReturnOne() {
		String actual = NumberTranslator.translateToEnglish(1);
		assertEquals("Expected result one.", "one", actual);
	}
	
	@Test
	public void NumberToEnglishShouldReturnTwo() {
		String actual = NumberTranslator.translateToEnglish(2);
		assertEquals("Expected result two.", "two", actual);
	}
	
	@Test
	public void NumberToEnglishShouldReturnThree() {
		String actual = NumberTranslator.translateToEnglish(3);
		assertEquals("Expected result three.", "three", actual);
	}
	
	@Test
	public void NumberToEnglishShouldReturnTwentyOne() {
		String actual = NumberTranslator.translateToEnglish(21);
		assertEquals("Expected result Twenty One.", "twenty one", actual);
	}
	
	@Test
	public void NumberToEnglishShouldReturnTwentyNine() {
		String actual = NumberTranslator.translateToEnglish(29);
		assertEquals("Expected result Twenty One.", "twenty nine", actual);
	}
	
	@Test
	public void NumberToEnglishShouldReturnThirty() {
		String actual = NumberTranslator.translateToEnglish(30);
		assertEquals("Expected result Thirty.", "thirty", actual);
	}
	
	@Test
	public void NumberToEnglishShouldReturnThirtyOne() {
		String actual = NumberTranslator.translateToEnglish(31);
		assertEquals("Expected result Thirty One.", "thirty one", actual);
	}
	
	@Test
	public void NumberToEnglishShouldReturn99() {
		String actual = NumberTranslator.translateToEnglish(99);
		assertEquals("Expected result Ninety Nine.", "ninety nine", actual);
	}

}
