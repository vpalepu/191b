package self.vpalepu.demo.bdd.step3;

import static org.junit.Assert.*;

import java.util.Arrays;
import java.util.Collection;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.junit.runners.Parameterized.Parameters;

@RunWith(Parameterized.class)
public class NumberTranslatorTeenTest {
	
	/*
	 * Scenario: Number between 13 and 19
	 *	Given number is less than equal to 13
	 *	and number greater than equal to 19
	 *	When translated to words,
	 *	Then translation ends with “teen”
	 *	and translation has one word.
	 */
	
	 // Given number is less than equal to 13
	 // and number greater than equal to 19
	@Parameters(name = "{index}: {0}={1}")
    public static Collection<Object[]> data() {
        return Arrays.asList(new Object[][] {
                 { 13, "thirteen" }, 
                 { 14, "fourteen" }, 
                 { 15, "fifteen" }, 
                 { 16, "sixteen" }, 
                 { 17, "seventeen" }, 
                 { 18, "eighteen" },
                 { 19, "nineteen" }  
           });
    }
    
    

    private int number;

    private String expectedTranslation;

    public NumberTranslatorTeenTest(int input, String expected) {
        number = input;
        expectedTranslation = expected;
    }
    
    @Test
    public void translationShouldBeAsExpected() {
    	// When
    	String translation = NumberTranslator.translateToEnglish(number);
    	// Then translation ends with “teen”
        assertEquals(expectedTranslation, translation);
    }
    
    @Test
    public void translationShouldContainOneWord() {
    	// When
    	String translation = NumberTranslator.translateToEnglish(number);
    	// Then ... and translation has one word.
        assertEquals(1, NumberTranslator.countWords(translation));
    }
    
    @Test
    public void translationShouldEndWithTeen() {
    	// When
    	String translation = NumberTranslator.translateToEnglish(number);
    	// Then translation is as expected.
    	assertTrue("Expected to end with 'teen'.", translation.endsWith("teen"));
    }
}
