package self.vpalepu.demo.bdd.step3;

/*
 * "Agile development methodologies, especially eXtreme Programming,
 * suggests that you do the simplest thing possible to get the thing working."
 * 
 * Actual Problem Statement: 
 * WAP to take an integer and turn it into words, in English. 
 * The rules for this may change depending on the language, 
 * so if English is not your only language, you may like to 
 * try to repeat this exercise in another language.
 * So, if the integer is 1, the result will be "one". 
 * If the integer is 23 the result will be "twenty three" and so on.
 * - "Test Driven / First Development by Example", Code Project
 * http://www.codeproject.com/Articles/18181/Test-Driven-First-Development-by-Example
 * 
 * Good resources for a crash course in software testing - 
 * JUnit Tutorial - http://www.vogella.com/articles/JUnit/article.html
 * Deciphering Testing Jargon http://net.tutsplus.com/tutorials/php/deciphering-testing-jargon/
 * "Jasmine" : JS Unit Testing framework - http://pivotal.github.com/jasmine/
 */

/*
 * User Story: Translate Numbers from Numerals to Words in English
 *	_As a_ Newspaper Editor,
 *	_I want to_ edit newspaper articles to translate numerals into actual words,
 *	_so that_ I can have the satisfaction of following some esoteric rule of English Grammar.
 */

public class NumberTranslator {
	
	// Step 2:
//	public static String translateToEnglish(int number) {
//		String string = null;
//		switch(number) {
//		case 1:
//			string = "one";
//			break;
//		case 2:
//			string = "two";
//			break;
//		case 3:
//			string = "three";
//			break;
//		case 4:
//			string = "four";
//			break;
//		case 5:
//			string = "five";
//			break;
//		case 6:
//			string = "six";
//			break;
//		case 7:
//			string = "seven";
//			break;
//		case 8:
//			string = "eight";
//			break;
//		case 9:
//			string = "nine";
//			break;
//		case 10:
//			string = "ten";
//			break;
//		case 11:
//			string = "eleven";
//			break;
//		case 12:
//			string = "twelve";
//			break;
//		case 13:
//			string = "thirteen";
//			break;
//		case 14:
//			string = "fourteen";
//			break;
//		case 15:
//			string = "fifteen";
//			break;
//		case 16:
//			string = "sixteen";
//			break;
//		case 17:
//			string = "seventeen";
//			break;
//		case 18:
//			string = "eighteen";
//			break;
//		case 19:
//			string = "nineteen";
//			break;
//		default:
//			break;
//		}
//		return string;
//	}

	// step 3:
	public static String translateToEnglish(int number) {
		if(number < 20) {
			return translate1To19(number);
		}

		String translation = null;
		
		int tens = number / 10;
		int units = number % 10;

		switch(tens) {
		case 2:
			translation = "twenty";
			break;
		case 3:
			translation = "thirty";
			break;
		case 4:
			translation = "forty";
			break;
		case 5:
			translation = "fifty";
			break;
		case 6:
			translation = "sixty";
			break;
		case 7:
			translation = "seventy";
			break;
		case 8:
			translation = "eighty";
			break;
		default:
			translation = "ninety";
			break;
		}

		if(units != 0) {
			translation = translation + " " + translate1To19(units);
		}
		return translation;
	}

	private static String translate1To19(int number) {
		String string = null;
		switch(number) {
		case 1:
			string = "one";
			break;
		case 2:
			string = "two";
			break;
		case 3:
			string = "three";
			break;
		case 4:
			string = "four";
			break;
		case 5:
			string = "five";
			break;
		case 6:
			string = "six";
			break;
		case 7:
			string = "seven";
			break;
		case 8:
			string = "eight";
			break;
		case 9:
			string = "nine";
			break;
		case 10:
			string = "ten";
			break;
		case 11:
			string = "eleven";
			break;
		case 12:
			string = "twelve";
			break;
		case 13:
			string = "thirteen";
			break;
		case 14:
			string = "fourteen";
			break;
		case 15:
			string = "fifteen";
			break;
		case 16:
			string = "sixteen";
			break;
		case 17:
			string = "seventeen";
			break;
		case 18:
			string = "eighteen";
			break;
		case 19:
			string = "nineteen";
			break;
		default:
			break;
		}
		return string;
	}
	
	public static int countWords(String translation) {
		String[] words = translation.split(" ");
		return words.length;
	}
}