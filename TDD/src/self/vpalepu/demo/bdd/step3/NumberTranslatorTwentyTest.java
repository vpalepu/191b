package self.vpalepu.demo.bdd.step3;

import static org.junit.Assert.*;

import java.util.Arrays;
import java.util.Collection;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.junit.runners.Parameterized.Parameters;

@RunWith(Parameterized.class)
public class NumberTranslatorTwentyTest {

	/*
	 * Scenario: Two digit number starts with 2
	 *	Given number has two digits
	 *	and number starts with 2
	 *	When translated to words,
	 *	Then translation starts with “twenty”.
	 */
	
	 // Given number has two digits
	 // and number starts with 2
	@Parameters(name = "{0}={1}")
    public static Collection<Object[]> data() {
        return Arrays.asList(new Object[][] {
                 { 20, "twenty" }, 
                 { 21, "twenty one" }, 
                 { 22, "twenty two" }, 
                 { 23, "twenty three" }, 
                 { 24, "twenty four" }, 
                 { 25, "twenty five" },
                 { 26, "twenty six" },
                 { 27, "twenty seven" },
                 { 28, "twenty eight" },
                 { 29, "twenty nine" },
           });
    }

    private int number;

    private String expectedTranslation;

    public NumberTranslatorTwentyTest(int input, String expected) {
        number = input;
        expectedTranslation = expected;
    }
    
    @Test
    public void translationShouldStartWithTwenty() {
    	// When
    	String translation = NumberTranslator.translateToEnglish(number);
    	// Then translation starts with “twenty”.
    	assertTrue("Expected to start with 'twenty'.", 
    			translation.startsWith("twenty"));
    }
    
    @Test
    public void translationShouldBeAsExpected() {
    	// When
    	String translation = NumberTranslator.translateToEnglish(number);
    	// Then translation is as expected.
        assertEquals(expectedTranslation, translation);
    }
}
